﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Etiqueta.Models.Propios
{
    public class MovimientoHome
    {
        public int MovimientoInventarioID { get; set; }
        public int MovimientoInventarioDetalleID { get; set; }
        public string NumeroMovimiento { get; set; }
        public System.DateTime Emision { get; set; }
        public string Codigo { get; set; }
        public string DescripcionItem { get; set; }
        public string DescripcionMovimiento { get; set; }
        public Nullable<decimal> Cantidad { get; set; }
        public Nullable<decimal> Peso { get; set; }
        public Nullable<decimal> CantidadAlterna { get; set; }
    }
}
