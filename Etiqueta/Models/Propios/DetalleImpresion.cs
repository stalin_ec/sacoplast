﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Etiqueta.Models.Propios
{
    public class DetalleImpresion
    {
        public string Codigo { get; set; }
        public string MovimientoId { get; set; }
        public string Producto { get; set; }
        public Nullable<decimal> PesoIdeal { get; set; }
        public Nullable<decimal> Peso { get; set; }
        public Nullable<decimal> PesoBruto { get; set; }
        public Nullable<decimal> Cantidad { get; set; }
        public string Turno { get; set; }
        public string Realizado { get; set; }
        public string Supervisor { get; set; }
        public string Prensa { get; set; }
        public string Cortador { get; set; }
        public string Comentario { get; set; }

        public Nullable<System.DateTime> FechaRegistro { get; set; }
    }
}
