﻿using Etiqueta.Controllers;
using Etiqueta.Models;
using Etiqueta.Models.EtiquetaData;
using Etiqueta.Models.Propios;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Etiqueta
{
    /// <summary>
    /// Lógica de interacción para EditarEtiqueta.xaml
    /// </summary>
    public partial class EditarEtiqueta : MetroWindow
    {
        private MovimientosInventarios movimientoPri { get; set; }
        public EditarEtiqueta()
        {
            InitializeComponent();
        }
        public EditarEtiqueta(MovimientoHome movimiento)
        {
            try
            {
                MovimientosController movimientosController = new MovimientosController();
                HomeController homeController = new HomeController();
                InitializeComponent();

                //this.movimientoPri = movimiento;

                cmbTurno.ItemsSource = homeController.GetTurnos();
                cmbTurno.DisplayMemberPath = "Descripcion";
                cmbTurno.SelectedValue = "TurnoId";

                string NumeroMovimiento = this.movimientoPri.NumeroMovimiento;

                cmbRealizado.ItemsSource = homeController.GetRealizados();
                cmbRealizado.DisplayMemberPath = "Descripcion";
                cmbRealizado.SelectedValue = "RealizadoId";

                dtgMovimientoSeries.Columns[0].IsReadOnly = true;
                dtgMovimientoSeries.Columns[1].IsReadOnly = true;
                Title = movimiento.NumeroMovimiento.ToString() + "  -  " + movimiento.DescripcionItem.ToString();

                //dtgMovimientoSeries.ItemsSource = movimientosController.GetMovimientosInventariosSeries(movimiento.MovimientoInventarioID);

                var c = movimientosController.GetMovimientosInventariosSeriesAll(movimiento.NumeroMovimiento);

                //var b = homeController.setEtiquetaDetalle(c, movimiento.Codigo, movimiento.DescripcionItem, NumeroMovimiento, movimiento.MovimientoInventarioDetalleID);
                //dtgMovimientoSeries.ItemsSource = b;
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        public EditarEtiqueta(MovimientosInventarios movimiento)
        {
            try
            {
                MovimientosController movimientosController = new MovimientosController();
                HomeController homeController = new HomeController();
                InitializeComponent();

                this.movimientoPri = movimiento;

                cmbTurno.ItemsSource = homeController.GetTurnos();
                cmbTurno.DisplayMemberPath = "Descripcion";
                cmbTurno.SelectedValue = "TurnoId";

                //string NumeroMovimiento = this.movimientoPri.NumeroMovimiento;

                cmbRealizado.ItemsSource = homeController.GetRealizados();
                cmbRealizado.DisplayMemberPath = "Descripcion";
                cmbRealizado.SelectedValue = "RealizadoId";

                dtgMovimientoSeries.Columns[0].IsReadOnly = true;
                dtgMovimientoSeries.Columns[1].IsReadOnly = true;
                dtgMovimientoSeries.Columns[2].IsReadOnly = true;
                dtgMovimientoSeries.Columns[3].IsReadOnly = true;
                Title = movimiento.NumeroMovimiento.ToString();

                //dtgMovimientoSeries.ItemsSource = movimientosController.GetMovimientosInventariosSeries(movimiento.MovimientoInventarioID);

                //var d = movimientosController.GetMovimientosInventariosSeries(movimiento.MovimientoInventarioID, movimiento.MovimientoInventarioDetalleID);

                var c = movimientosController.GetMovimientosInventariosSeriesAll(movimiento.NumeroMovimiento);

                var b = homeController.setEtiquetaDetalle(c);
                dtgMovimientoSeries.ItemsSource = b;
            }
            catch (Exception ex)
            {

                throw;
            }

        }



        public bool ValidateCampos()
        {
            return true;
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (cmbRealizado.SelectedIndex == -1)
                {
                    MessageBox.Show("Debe escoger un encargado");
                    cmbRealizado.Focus();
                }
                else if (cmbTurno.SelectedIndex == -1)
                {
                    MessageBox.Show("Debe escoger un Turno");
                    cmbTurno.Focus();
                }
                else
                {
                    #region Guardar Cabecera
                    ETIQUETACABECERA cabecera = new ETIQUETACABECERA();
                    cabecera.MovimientoId = movimientoPri.NumeroMovimiento;
                    //cabecera.Cantidad = movimientoPri.Cantidad;
                    //cabecera.CantidadAlterna = movimientoPri.CantidadAlterna;
                    //cabecera.Codigo = movimientoPri.Codigo;
                    cabecera.Cortador = txtCortador.Text;
                    cabecera.Prensa = txtPrensa.Text;
                    //cabecera.Producto = movimientoPri.DescripcionItem;
                    var realizado = cmbRealizado.SelectedValue as Realizado;
                    cabecera.RealizadoId = realizado.RealizadoId;
                    cabecera.Supervisor = txtSupervisor.Text;
                    //cabecera.TipoMovimiento = movimientoPri.DescripcionMovimiento;
                    var turno = cmbTurno.SelectedValue as Turno;
                    cabecera.TurnoId = turno.TurnoId;
                    #endregion
                
                    HomeController homeController = new HomeController();
                    homeController.UpdateOrSaveCabecera(cabecera);
                    homeController.UpdateCabecera(movimientoPri.NumeroMovimiento);

                    var a = homeController.detalleImpresions(movimientoPri.NumeroMovimiento);
                    a.ForEach(detalle => {
                        Impresion impresion = new Impresion(detalle);
                        impresion.Show();
                        impresion.Close();
                    });
                }
                

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al Guardar Por favor intentelo nuevamente");
            }
            

        }

        private void dtgMovimientoSeries_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            try
            {
                var a = e.Row;
                DataRowView rowView = e.Row.Item as DataRowView;
                TextBox t = e.EditingElement as TextBox;
                var resultado = dtgMovimientoSeries.SelectedItem;
                ETIQUETADETALLE editarEtiqueta = (ETIQUETADETALLE)resultado;

                HomeController homeController = new HomeController();
                homeController.UpdateDetalle(editarEtiqueta);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al Guardar Por favor intentelo nuevamente");
            }
            
        }

        private void dtgMovimientoSeries_BeginningEdit(object sender, DataGridBeginningEditEventArgs e)
        {
            try
            {
                var resultado = dtgMovimientoSeries.SelectedItem;
                ETIQUETADETALLE editarEtiqueta = (ETIQUETADETALLE)resultado;
                HomeController homeController = new HomeController();
                homeController.UpdateDetalle(editarEtiqueta);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        private void dtgMovimientoSeries_FocusableChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            
        }

        private void dtgMovimientoSeries_CurrentCellChanged(object sender, EventArgs e)
        {
            /*var resultado = dtgMovimientoSeries.SelectedItem;
            ETIQUETADETALLE editarEtiqueta = (ETIQUETADETALLE)resultado;
            HomeController homeController = new HomeController();
            homeController.UpdateDetalle(editarEtiqueta);*/
        }

        private void dtgMovimientoSeries_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                var resultado = dtgMovimientoSeries.SelectedItem;
                ETIQUETADETALLE editarEtiqueta = (ETIQUETADETALLE)resultado;
                HomeController homeController = new HomeController();
                homeController.UpdateDetalle(editarEtiqueta);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        private void dtgMovimientoSeries_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            try
            {
                var resultado = dtgMovimientoSeries.SelectedItem;
                ETIQUETADETALLE editarEtiqueta = (ETIQUETADETALLE)resultado;
                HomeController homeController = new HomeController();
                homeController.UpdateDetalle(editarEtiqueta);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        private void dtgMovimientoSeries_TextInput(object sender, TextCompositionEventArgs e)
        {
            
        }

        private void dtgMovimientoSeries_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            /*var resultado = dtgMovimientoSeries.SelectedItem;
            ETIQUETADETALLE editarEtiqueta = (ETIQUETADETALLE)resultado;
            HomeController homeController = new HomeController();
            homeController.UpdateDetalle(editarEtiqueta);
            */
        }

        private void dtgMovimientoSeries_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                var resultado = dtgMovimientoSeries.SelectedItem;
                ETIQUETADETALLE editarEtiqueta = (ETIQUETADETALLE)resultado;
                HomeController homeController = new HomeController();
                homeController.UpdateDetalle(editarEtiqueta);
            }
            catch (Exception ex)
            {
                dtgMovimientoSeries.SelectedIndex = 0;
            }

        }

        private void btnSalir_Click(object sender, RoutedEventArgs e)
        {
            /*try
            {
                HomeController homeController = new HomeController();
                var a = homeController.detalleImpresions(this.movimientoPri.NumeroMovimiento, this.movimientoPri.MovimientoInventarioDetalleID);
                a.ForEach(detalle => {
                    Impresion impresion = new Impresion(detalle);
                    impresion.ShowDialog();
                });
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en la impresion");
            }*/
            
        }

        private void dtgMovimientoSeries_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                var a = (char)e.Key;
                if (!(char.IsDigit((char)e.Key) || (char)e.Key == (char)Key.Back || (char)e.Key == '.' || (char)e.Key == (char)Key.Tab || (char)e.Key == (char)Key.Enter))
                {
                    e.Handled = false;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            
        }
    }
}
