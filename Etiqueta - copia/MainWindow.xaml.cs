﻿using Etiqueta.Controllers;
using Etiqueta.Models.Propios;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Etiqueta
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        private static Mutex mutex;
        private static bool PrimeraInstancia
        {
            get
            {
                string fullName = Assembly.GetEntryAssembly().FullName;
                bool result;
                MainWindow.mutex = new Mutex(true, fullName, out result);
                return result;
            }
        }
        public MainWindow()
        {
            InitializeComponent();
            if (!MainWindow.PrimeraInstancia)
            {
                MessageBox.Show("Ya se encuentra una instancia en Ejecución");
                Environment.Exit(0);
            }
        }

        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            MovimientosController movimientosController = new MovimientosController();

            HomeController homeController = new HomeController();


            try
            {
                var a = homeController.VerificaGuardado(txtDocumento.Text);
                if (a)
                {
                    MessageBox.Show("Registro ya Guardado o Editado");
                }
                else
                {
                    var resultado = movimientosController.movimientosInventarios(txtDocumento.Text);
                    if (resultado.Count == 0)
                    {
                        MessageBox.Show("No existen Movimientos para la busqueda");
                        dtgMovimientos.ItemsSource = null;
                    }
                    else
                    {
                        dtgMovimientos.ItemsSource = resultado;
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("No existen Movimientos para la busqueda","Sin resultados");
            }
            

            //var resultado2 = movimientosController.movimientosInventarios();
            
        }

        private void DataGridRow_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var resultado = dtgMovimientos.SelectedItem;
                EditarEtiqueta editarEtiqueta = new EditarEtiqueta((MovimientoHome)resultado);
                editarEtiqueta.ShowDialog();
            }
            catch (Exception)
            {

                throw;
            }
            
        }

        private void btnReimprimir_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                HomeController homeController = new HomeController();
                var b = homeController.VerificaGuardado(txtDocumento.Text);
                if (b)
                {
                    var a = homeController.detalleImpresions(txtDocumento.Text);
                    /*a.ForEach(detalle => {
                        //Impresion impresion = new Impresion(detalle);
                        //impresion.Show();
                        //impresion.Close();

                    });*/
                    homeController.UpdateAuditoriaReimpresion(txtDocumento.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en la impresion");
            }
        }
    }
}
