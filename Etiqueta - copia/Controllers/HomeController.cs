﻿using Etiqueta.Models;
using Etiqueta.Models.EtiquetaData;
using Etiqueta.Models.Propios;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Etiqueta.Controllers
{
    public class HomeController
    {
        public List<Turno> GetTurnos ()
        {
            ETIQUETASEntities db = null;
            List<Turno> turnos = new List<Turno>();
            using(db = new ETIQUETASEntities())
            {

                turnos = db.Turno.ToList();
            }
            return turnos;
        }

        public List<Realizado> GetRealizados()
        {
            ETIQUETASEntities db = null;
            List<Realizado> realizados = new List<Realizado>();
            using(db = new ETIQUETASEntities())
            {
                realizados = db.Realizado.ToList();
            }
            return realizados;
        }

        public List<ETIQUETADETALLE> setEtiquetaDetalle(List<MovimientosInventariosSeries> series, string codigo, string NombreProducto, string NumeroMovimiento, int MovimientoInventarioDetalleId)
        {

            List<ETIQUETADETALLE> resultado = new List<ETIQUETADETALLE>();

            List<ETIQUETADETALLE> a = new List<ETIQUETADETALLE>();

            var db = new ETIQUETASEntities();

            a = db.ETIQUETADETALLE.Where(x => x.EtiquetaId == NumeroMovimiento).Where(x=>x.MovimientoInventarioDetalleId==MovimientoInventarioDetalleId).ToList();
            if(a.Count > 0)
            {
                return a;
            }
            else
            {
                series.ForEach(dto => {
                    var r = new ETIQUETADETALLE();
                    r.EtiquetaId = NumeroMovimiento;
                    r.MovimientoInventarioDetalleId = MovimientoInventarioDetalleId;
                    r.Cantidad = dto.Cantidad;
                    r.Peso = dto.Peso;
                    r.Codigo = codigo;
                    r.Producto = NombreProducto;
                    resultado.Add(r);
                    db.ETIQUETADETALLE.Add(r);
                    db.SaveChanges();
                });
                return resultado;
            }   
        }

        public void UpdateDetalle(ETIQUETADETALLE etiqueta)
        {
            ETIQUETASEntities db = null;
            using (db = new ETIQUETASEntities())
            {
                var result = db.ETIQUETADETALLE.SingleOrDefault(b => b.EtiquetaDetalleId == etiqueta.EtiquetaDetalleId);
                if (result != null)
                {
                    result.PesoBruto = etiqueta.PesoBruto;
                    result.PesoIdeal = etiqueta.PesoIdeal;
                    result.Comentario = etiqueta.Comentario;
                    //db.Entry(result).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
        }

        public void UpdateAuditoriaReimpresion(string MovimientoId)
        {
            ETIQUETASEntities db = null;
            using(db = new ETIQUETASEntities())
            {
                AUDITORIAREIMPRESION a = new AUDITORIAREIMPRESION();
                a.Fecha = DateTime.Now;
                a.MovimientoId = MovimientoId;
                a.Maquina = System.Environment.MachineName.ToString();
                db.AUDITORIAREIMPRESION.Add(a);
                db.SaveChanges();
            }
        }

        public void UpdateContadorReimpresion(string MovimientoId)
        {
            ETIQUETASEntities db = null;
            using (db = new ETIQUETASEntities())
            {
                var result = db.ETIQUETACABECERA.SingleOrDefault(b => b.MovimientoId == MovimientoId);
                if (result != null)
                {
                    result.ContadorReimpresion = result.ContadorReimpresion + 1;
                    //db.Entry(result).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
        }

        public bool UpdateOrSaveCabecera(ETIQUETACABECERA cabecera)
        {
            ETIQUETASEntities db = null;
            using (db = new ETIQUETASEntities())
            {
                var result = db.ETIQUETACABECERA.SingleOrDefault(b => b.MovimientoId == cabecera.MovimientoId);
                if (result != null)
                {
                    result.Prensa = cabecera.Prensa;
                    result.Cortador = cabecera.Cortador;
                    result.RealizadoId = cabecera.RealizadoId;
                    result.Prensa = cabecera.Prensa;
                    result.TurnoId = cabecera.TurnoId;
                    //db.Entry(result).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    db.ETIQUETACABECERA.Add(cabecera);
                    db.SaveChanges();
                }
            }
            return true;
        }

        public void UpdateCabecera(string movimientoId)
        {
            ETIQUETASEntities db = null;
            using (db = new ETIQUETASEntities())
            {
                var result = db.ETIQUETACABECERA.SingleOrDefault(b => b.MovimientoId == movimientoId);
                if (result != null)
                {
                    result.Actualizada = 1;
                    result.FechaRegistro = DateTime.Now;
                    //db.Entry(result).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
        }

        public void SaveCabecera(ETIQUETACABECERA cabecera)
        {
            ETIQUETASEntities db = null;
            {
                using(db = new ETIQUETASEntities())
                {
                    db.ETIQUETACABECERA.Add(cabecera);
                    db.SaveChanges();
                }
            }
        }

        public List<ETIQUETADETALLE> getEtiquetaDetalle()
        {
            List<ETIQUETADETALLE> e = new List<ETIQUETADETALLE>();
            ETIQUETASEntities db = null;
            using(db = new ETIQUETASEntities())
            {
                var resultado = db.ETIQUETADETALLE;
                e = resultado.ToList();
            }
            return e;
        }

        public bool VerificaGuardado(string MovimientoId)
        {
            var db = new ETIQUETASEntities();
            var a = db.ETIQUETACABECERA.Where(x => x.MovimientoId == MovimientoId).Where(x=>x.Actualizada ==1).ToList();
            if(a.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<DetalleImpresion> detalleImpresions (string MovimientoId, int MovimientoInventarioDetalleId)
        {
            List<DetalleImpresion> impresion = new List<DetalleImpresion>();
            ETIQUETASEntities db = null;
            using(db = new ETIQUETASEntities())
            {
                var resultado = from cabecera in db.ETIQUETACABECERA
                                join detalle in db.ETIQUETADETALLE on cabecera.MovimientoId equals detalle.EtiquetaId
                                join turno in db.Turno on cabecera.TurnoId equals turno.TurnoId
                                join realizado in db.Realizado on cabecera.RealizadoId equals realizado.RealizadoId
                                where detalle.EtiquetaId == MovimientoId where detalle.MovimientoInventarioDetalleId == MovimientoInventarioDetalleId 
                                select new DetalleImpresion {
                                    Codigo = detalle.Codigo,
                                    MovimientoId = cabecera.MovimientoId,
                                    Producto = detalle.Producto,
                                    PesoIdeal = detalle.PesoIdeal,
                                    PesoBruto = detalle.PesoBruto,
                                    Cantidad = detalle.Cantidad,
                                    Turno = turno.Descripcion,
                                    Realizado = realizado.Descripcion,
                                    Peso = detalle.Peso,
                                    Cortador = cabecera.Cortador,
                                    Prensa = cabecera.Prensa,
                                    Supervisor = cabecera.Supervisor,
                                    Comentario = detalle.Comentario,
                                    FechaRegistro = cabecera.FechaRegistro
                                };
                impresion = resultado.ToList();
            }
            return impresion;
        }
        public List<DetalleImpresion> detalleImpresions(string MovimientoId)
        {
            List<DetalleImpresion> impresion = new List<DetalleImpresion>();
            ETIQUETASEntities db = null;
            using (db = new ETIQUETASEntities())
            {
                var resultado = from cabecera in db.ETIQUETACABECERA
                                join detalle in db.ETIQUETADETALLE on cabecera.MovimientoId equals detalle.EtiquetaId
                                join turno in db.Turno on cabecera.TurnoId equals turno.TurnoId
                                join realizado in db.Realizado on cabecera.RealizadoId equals realizado.RealizadoId
                                where detalle.EtiquetaId == MovimientoId
                                select new DetalleImpresion
                                {
                                    Codigo = detalle.Codigo,
                                    MovimientoId = cabecera.MovimientoId,
                                    Producto = detalle.Producto,
                                    PesoIdeal = detalle.PesoIdeal,
                                    PesoBruto = detalle.PesoBruto,
                                    Cantidad = detalle.Cantidad,
                                    Turno = turno.Descripcion,
                                    Realizado = realizado.Descripcion,
                                    Peso = detalle.Peso,
                                    Cortador = cabecera.Cortador,
                                    Prensa = cabecera.Prensa,
                                    Supervisor = cabecera.Supervisor,
                                    Comentario = detalle.Comentario,
                                    FechaRegistro = cabecera.FechaRegistro
                                };
                impresion = resultado.ToList();
            }
            return impresion;
        }

    }
}
