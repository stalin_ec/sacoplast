﻿using Etiqueta.Models;
using Etiqueta.Models.Propios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Etiqueta.Controllers
{
    public class MovimientosController
    {
        public List<MovimientoHome> movimientosInventarios ()
        {
            SACO_0990868107001Entities db = null;
            List<MovimientoHome> movimientosInventariosList = new List<MovimientoHome>();
            using (db = new SACO_0990868107001Entities())
            {
                var resultado = from movimiento in db.MovimientosInventarios
                                join detalle in db.MovimientosInventariosDetalles on movimiento.MovimientoInventarioID equals detalle.MovimientoInventarioID
                                join tipo in db.TiposMovInventario on movimiento.TipoMovInventarioID equals tipo.TipoMovInventarioID
                                join item in db.Items on detalle.ItemID equals item.ItemID
                                where tipo.NaturalezaAsiento == "I"
                                where movimiento.NumeroMovimiento == "01-20-000000002"
                                select new MovimientoHome {
                                    NumeroMovimiento = movimiento.NumeroMovimiento,
                                    Emision = movimiento.Emision,
                                    Codigo = item.Codigo,
                                    DescripcionItem = item.Descripcion,
                                    DescripcionMovimiento = tipo.Descripcion,
                                    Cantidad = detalle.Cantidad,
                                    CantidadAlterna = detalle.CantidadAlterna
                                };
                movimientosInventariosList = resultado.ToList();
            }
            return movimientosInventariosList;
        }

        public List<MovimientoHome> movimientosInventarios(string movimientoNumero)
        {
            SACO_0990868107001Entities db = null;
            List<MovimientoHome> movimientosInventariosList = new List<MovimientoHome>();
            using (db = new SACO_0990868107001Entities())
            {
                var resultado = from movimiento in db.MovimientosInventarios
                                join detalle in db.MovimientosInventariosDetalles on movimiento.MovimientoInventarioID equals detalle.MovimientoInventarioID
                                join tipo in db.TiposMovInventario on movimiento.TipoMovInventarioID equals tipo.TipoMovInventarioID
                                join item in db.Items on detalle.ItemID equals item.ItemID
                                where tipo.NaturalezaAsiento == "I"
                                where movimiento.NumeroMovimiento == movimientoNumero
                                select new MovimientoHome
                                {
                                    MovimientoInventarioID = movimiento.MovimientoInventarioID,
                                    NumeroMovimiento = movimiento.NumeroMovimiento,
                                    MovimientoInventarioDetalleID = detalle.MovimientoInventarioDetalleID,
                                    Emision = movimiento.Emision,
                                    Codigo = item.Codigo,
                                    DescripcionItem = item.Descripcion,
                                    DescripcionMovimiento = tipo.Descripcion,
                                    Cantidad = detalle.Cantidad,
                                    CantidadAlterna = detalle.CantidadAlterna
                                };
                movimientosInventariosList = resultado.ToList();
            }
            return movimientosInventariosList;
        }

        public List<MovimientosInventariosSeries> GetMovimientosInventariosSeries(int MovimientoId, int DetalleId)
        {
            SACO_0990868107001Entities db = null;
            List<MovimientosInventariosSeries> movimientosInventariosSeries = new List<MovimientosInventariosSeries>();
            {
                using(db = new SACO_0990868107001Entities())
                {
                    var resultado = from movimientoSerie in db.MovimientosInventariosSeries
                                    where movimientoSerie.MovimientoInventarioID == MovimientoId
                                    where movimientoSerie.MovimientoInventarioDetalleID == DetalleId
                                    select movimientoSerie;
                    movimientosInventariosSeries = resultado.ToList();
                }
            }
            return movimientosInventariosSeries;
        }
    }
}
