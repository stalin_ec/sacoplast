﻿using Etiqueta.Models.Propios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Etiqueta
{
    /// <summary>
    /// Lógica de interacción para Impresion.xaml
    /// </summary>
    public partial class Impresion : Window
    {
        private DetalleImpresion DetalleImpresionPRI { get; set; }

        



        public Impresion()
        {
            InitializeComponent();
        }

        public Impresion(DetalleImpresion detalle)
        {
            InitializeComponent();
            this.Visibility = System.Windows.Visibility.Visible;
            this.DetalleImpresionPRI = detalle;
            LlenarCampos();
            PrintDialog printDlg = new PrintDialog();
            printDlg.PrintVisual(gridImpresion, "Imprimiendo");
            this.Visibility = System.Windows.Visibility.Hidden;
            //this.Close();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            ///this.Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }
        public void LlenarCampos()
        {
            txtCodigo.Text = this.DetalleImpresionPRI.Codigo.ToString();
            txtMovimiento.Text = this.DetalleImpresionPRI.MovimientoId;
            txtProducto.Text = this.DetalleImpresionPRI.Producto;
            txtPesoIdeal.Text = this.DetalleImpresionPRI.PesoIdeal?.ToString("F");
            txtPeso.Text = this.DetalleImpresionPRI.Peso?.ToString("F");
            txtPesoBruto.Text = this.DetalleImpresionPRI.PesoBruto?.ToString("F");
            txtCantidad.Text = this.DetalleImpresionPRI.Cantidad?.ToString("F");
            txtTurno.Text = this.DetalleImpresionPRI.Turno;
            txtPrensa.Text = this.DetalleImpresionPRI.Prensa;
            txtCortador.Text = this.DetalleImpresionPRI.Cortador;
            txtSupervisor.Text = this.DetalleImpresionPRI.Supervisor;
            txtObservaciones.Text = this.DetalleImpresionPRI.Comentario;
            txtFecha.Text = this.DetalleImpresionPRI.FechaRegistro?.ToString("dd/MM/yyyy");
            txtHora.Text = this.DetalleImpresionPRI.FechaRegistro?.ToString("HH:mm");
            //PrintDialog printDlg = new PrintDialog();
            //printDlg.PrintVisual(gridImpresion, "Imprimiendo");

            //this.Close();
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            
        }
    }
}
