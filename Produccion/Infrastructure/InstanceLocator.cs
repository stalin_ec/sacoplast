﻿
using Produccion.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Produccion.Infrastructure
{
    public class InstanceLocator
    {
        public MainWindow Main { get; set; }
        public BilaminacionModel Bilaminacion { get; set; }
        public InstanceLocator() 
        {
            Main = new MainWindow();
            Bilaminacion = new BilaminacionModel();
        }
    }
}
