﻿using GalaSoft.MvvmLight.Command;
using Produccion.Pages;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Produccion.ViewModel
{
    public class MainModel : INotifyPropertyChanged
    {
        #region Eventos
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Comandos
        public ICommand CargarExtrusionCommand { get { return new RelayCommand(CargarExtrusion); } }

        private void CargarExtrusion()
        {
            ExtrusionPage Extrusion = new ExtrusionPage();
            Extrusion.ShowDialog();
        }
        #endregion
    }
}