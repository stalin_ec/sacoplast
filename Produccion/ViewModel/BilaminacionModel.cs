﻿using GalaSoft.MvvmLight.Command;
using Produccion.Pages;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Produccion.ViewModel
{
    public class BilaminacionModel : INotifyPropertyChanged
    {
        
        #region Eventos
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Comandos
        public ICommand CargarBilaminacionCommand { get { return new RelayCommand(CargarBilaminacion); } }

        private void CargarBilaminacion()
        {
            BilaminacionPage Bilaminacion = new BilaminacionPage();
            Bilaminacion.ShowDialog();
        }
        #endregion
    }
}
